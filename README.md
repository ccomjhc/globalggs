# README #


### What is this repository for? ###

15 October 2019


The source code in this repository represents a proof-of-concept implementation of the GGGS described in the publication: A Proposed Global Geographic Grid System for Visualizing Bathymetry.

The implementation was done in C++ and OpenGL and uses glut to provide a GUI for the test driver. Naturally, it could be implemented in any programming language and there are many possible variations. This code is included as a working example only.  Any full immplementation will require a file structure to store data files as well as support for whatever gridded terrain model formats are most appropriate.  However, the examples given may be taken as a starting point to be extended according to the projectx requirements. APPENDIX A (included here) contains a description of the major classes and methods.

Also included are sample data files These include 8 deg files from the GEBCO 2014 release of a global bathymetry grid.
In addition high resolution 1 deg files were derived from the GMRT database.  

The software is written in c++ and uses openGL with GLUT for the GUI. It is issued with a BSD open source license.
This version was build using a freeglut library. The data files make use of PNG compression.


### Who do I talk to? ###

Contact Colin Ware: cware@ccom.unh.edu