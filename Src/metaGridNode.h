

#ifndef META_GRID_H
#define META_GRID_H

#include "structs.h"
#include <vector>
#include "bathyGrid.h"

using namespace std;

class metaGridNode
{

public:
	metaGridNode(int mg8Lat, double locLon, double locLat, 
		double LLon, double LLat,int depth);
	// This method recursively builds the tree (unless it already exists) until it gets to the appropriate spatial location and depth
	// then it inserts the data grid at that location
	bool insertChild(char *fname, double focusLat,double focusLon, 
				 int childC, 
				int childR,int depth,int Tdepth);
	// The rendering method. Recursively traverses the metaGrid tree and draws dems where they exist
	void drawTree(int depth,  bool lores);
	void drawTreeAreas(int dep);
	// link to neighbors (sibs and cousins)
	void linkSibs(int mg8Lat,metaGridNode *sE,metaGridNode *sW,
			metaGridNode *sN,metaGridNode *sS);	
	// This is used for a particular kind of multibeam data (from GMRT) This data has gaps between swaths
	// Pushdown fills these gaps with lower resolution data.
	void pushDown(int depth);
	//Splits a dem into four parts and pushes them lower in the tree
	// Purpose is to fill around higher resolution data
	void insertDtmQuadrant(bathyGrid *dtm,int child);

	// Builds a data structure for use in stitching together adjacent dems.
	void buildStitchLists();
	// Used by  buildStitchLists to recursively find all neighboring nodes along a 
	// particular edge
	void westEdge(bathyGrid::stitchList2 *es,int edgeSize, int edgeStart);
	void eastEdge(bathyGrid::stitchList2 *ws, int edgeSize, int edgeStart);
	void southEdge(bathyGrid::stitchList2 *ss, int edgeSize, int edgeStart);
	void northEdge(bathyGrid::stitchList2 *ns, int edgeSize, int edgeStart);
	void getSWcorner(p3d &corner);

	void printChildren();	// for debugging
	void printStitchLists();// for debugging

	// for the special case stitch lists at the 72 and 80 deg boundaries
	void linkSouthEdge72_80(metaGridNode *s1, metaGridNode *s2, metaGridNode *s3);
	void gen_72_80_BorderStrip();  // used for south of 80, or 72
	void getSPoints(vector<p3d> &sPointList,int &sPointCount);
	void getNPoints(vector<p3d> &nPointList,int &nPointCount);
	// for the special case stitch lists at the 72 and 80 deg boundaries
	void genTriangleList();
	void drawTriangleList();

	int nodeID;
	bool hasChildren;
private:
	int thisDepth;

	double localLat, localLon;	// the reference location  change to focusLat
	double LL_Lon, LL_Lat;		// the lower left corner of this node;
	int frame8Lon, frame8Lat;	// used in determining the file name
	float relLon, relLat;		// the relative lon, lat of the LL corner.
	double nodeWidth, nodeHeight;

	float lonMultiplier;	// used for northern metagrid nodes
	bathyGrid *dtm;			// the local terrain map
	metaGridNode *children[4];
	metaGridNode *Nsib, *Ssib, *Esib, *Wsib;// tree threading
	metaGridNode *Ssib2, *Ssib3;  // for south of 72 and south of 80

	bathyGrid::stitchList2 *estitch, *wstitch, *nstitch, *sstitch; 
	int esCount;

	p3d swCorner;

	vector <p3d> northPointList;  // used for south of 80 and south of 72
	vector <p3d> southPointList;
	vector <triangle> triangleList;
	int northPointCount, southPointCount;
};

#endif 

