/*-----------------------------------------------------------------------------------------
Copyright(c) 2019, 2019 University of New Hampshire, Center for Coastal and Ocean Mapping,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met :
*Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and / or other materials provided with the distribution.
* Neither the name of the <organization> nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************************/
////////////////////////////////////////////////////////////////
//
// GGGSroot.h: The root of the GGGS metagrid system. The globe is divided into 
// 8x8 deg regions between +/- 72 deg. 	
	// the columns start at -180
	// the rows start at -96
// For regions closer to the poles the grid cells have greater widths in Longitude. 
// N/S of 72 width = 24deg. N/S of 80 width = 72 deg.See publication for details.
// Part of an implementation of the Global Geographic Grid System (GGGS).
// Part of an implementation of the Global Geographic Grid System (GGGS).
//
//////////////////////////////////////////////////////////////////////

#ifndef MG_ROOT_H
#define MG_ROOT_H

class metaGridNode;
class cmap;

class GGGSroot
{

public:
	GGGSroot();
	void loadData(float Lon, float Lat,float width, float height); // A test driver specific to the
																	// test data provided
	void draw(bool lores);
	void loadColorMaps();
private:
	void calc_1degFname(int Lon, int Lat, char *fname);	// specific to the files used in this
														// implementation
	void getM8coords(float refLon, float refLat, int &Row, int &Col); // determine
							// The position in the globalGrid of the quad tree root
							// for a particular location

	void GGGSroot::genSibs(int mg8lat, int mg8lon,
		metaGridNode **Es,metaGridNode **Ws,metaGridNode **Ss,metaGridNode **Ns,
		metaGridNode **Ss2, metaGridNode **Ss3);  // Starts the process of
						// linking siblings in the metaGrid forest

	metaGridNode *globalGrid[24][45]; // The global grid structure  of 8x8 deg squares

	int mgRowStart, mgColStart, mgColEnd, mgRowEnd;
	
	cmap *colormap;

	float focusLat, focusLon;  // a local reference used in defining the view in rendering
	// all internal positions are defined relative to this to remove the need for double precision
	float currentViewWid; // the view width in degrees
};

#endif 


