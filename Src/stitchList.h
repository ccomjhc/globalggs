#include "structs.h"

#ifndef STITCHLIST_H
#define STITCHLIST_H

class stitchList
{
public:
	stitchList();
	float *dtmStrips[8]; // an array of up to 8 dem strips
	float *Xstrips[8];
	float *Ystrips[8];
	vNorms *normStrips[8];
	int demStarts[8];
	int nids[8]; // the node ids of the strips
	int npts[8];  // the number of points in this strip
	//int demRes;
	int count;
private:

};

#endif;