
#ifndef STRUCTS_H
#define STRUCTS_H

typedef short vNorms[3];
typedef float Point3d[3];

struct p3d
{
	Point3d pt;
	vNorms pn;
	short attrib;
};

struct triangle
{
	Point3d verts[3];
	vNorms vertNorms[3];
	short att[3];
};

// perhaps refactor
/*
struct stitchList
{
	float *dtmStrips[8]; // an array of up to 8 dem strips
	float *Xstrips[8];
	float *Ystrips[8];
	vNorms *normStrips[8];
	int demStarts[8];
	int nids[8]; // the node ids of the strips
	int npts[8];  // the number of points in this strip
	//int demRes;
	int count;

};

*/
#endif 
