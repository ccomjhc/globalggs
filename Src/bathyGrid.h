/*-----------------------------------------------------------------------------------------
Copyright(c) 2019, 2019 University of New Hampshire, Center for Coastal and Ocean Mapping,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met :
*Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and / or other materials provided with the distribution.
* Neither the name of the <organization> nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************************/
////////////////////////////////////////////////////////////////
//
// BathyGrid.h: A regular grid of bathymetric depths
// For use within the framework of the Global Geographic Grid System (GGGS).
//
//////////////////////////////////////////////////////////////////////

#ifndef BATHY_GRID_H
#define BATHY_GRID_H

#include "structs.h"
#include "stitchList.h"
#include <vector>

using namespace std;

#define VERT_SCALE	0.000035f  // these are arbitrary scale factors to make the dems look OK
#define TEX_SCALE 0.0002f

#define SHADE_SCALE 0.00005f;
#define MAX_PTS 2000

#define MAX_NEIGHBORS 8 // the maximum neighbors for stitch lists

class readPNG;
class cmap;


class bathyGrid 
{
public:

	// the stitch list data structure
	struct stitchList2 
	{
		bathyGrid *dtmSList[MAX_NEIGHBORS]; // an array of up to 8 dem 
		int demStarts[MAX_NEIGHBORS];		// start pos relative to this node
								//int nids[8]; // the node ids of the strips
		int npts[MAX_NEIGHBORS];			// the number of points in this strip
					
		int count;
	private:

	};

	bathyGrid(double LLon,double LLat, double height, float widMultiplier, int nID);	
	~bathyGrid();

	// The dem rendering method
	void DrawDem(bool lores);
	// Load a compressed bathy file
	bool loadPNGbathy(char *fname,double focLat, float relLat,float relLon, int cmap);	

	// Reprojects from Lat Lon coordinates to creates a an orthographic projection 
	// on a plane tangential to the earth at the specified focus point
	void computeXYfromLatLon(double focLat, float rLon, float rLat, bool print);

	bool addDTMquadrant(bathyGrid *ddtm,int child);
	bool dtmQuadrantMerge(bathyGrid *ddtm,int child);
	int  allocateMem();

	void copySouthEdgeV(vector<p3d> &southPointList,int &southPointCount);
	void copyNorthEdgeV(vector<p3d> &southPointList,int &southPointCount);

	// Draw the stitch lists to fill the gaps between dems.
	void DrawEastEdge2(stitchList2 *east, bool lores);
	void DrawWestEdge2(stitchList2 *west,  bool lores);
	void DrawNorthEdge2(stitchList2 *north,bool lores);
	void DrawSouthEdge2(stitchList2 *south, bool lores);

	void getSWcornerVal(p3d &pt);
	void DrawArea(float relLon,float relLat);

	float cellSize;	// the size of a dem grid cell in Lat Lon units
	int nY; // the gridsize
	int nX;	
	double LL_Lat, LL_Lon; //lower left corner
	float relLat, relLon;  // relative to localRef	
	int nodeID; // for debugging
protected:

	int inrows, incols;	
	void CalculateNormals2();

	float widthMultiplier;
	double width,height;  // grid width in degrees

	readPNG *pngread;

	float **dem;	// a 2D grid of height values
	float **mapX;	// 2D projection of a lat,lon mesh
	float **mapY;

	vNorms **norms_array;	// Surfac Normals
	unsigned char **attrib;	// Surface Attributes

	float maxheight;
	float minheight;

	int noData;
};

#endif 
