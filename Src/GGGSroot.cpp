/*-----------------------------------------------------------------------------------------
Copyright(c) 2019, 2019 University of New Hampshire, Center for Coastal and Ocean Mapping,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met :
*Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and / or other materials provided with the distribution.
* Neither the name of the <organization> nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************************/
////////////////////////////////////////////////////////////////
//
// GGGSroot.cpp: The root of the GGGS metagrid system. The globe is divided into 
// 8x8 deg regions between +/- 72 deg. 	
// the columns start at -180
// the rows start at -96
// For regions closer to the poles the grid cells have greater widths in Longitude. 
// N/S of 72 width = 24deg. N/S of 80 width = 72 deg.See publication for details.
// Part of an implementation of the Global Geographic Grid System (GGGS).
//
//////////////////////////////////////////////////////////////////////
#include "GGGSroot.h"
#include <stdlib.h>
#include "metaGridNode.h"
#include <iostream>
#include <GL/glut.h>
#include <math.h>
#include "cmap.h"

using namespace std;

GGGSroot::GGGSroot()
{
	int r,c;
	for(r=0;r<24;++r)
		for(c=0;c<45;++c)
			globalGrid[r][c] = NULL;

	focusLat = focusLon = 0.0f;
	currentViewWid = 3.0;
}


// The loadData class is a test Driver designed to load the specific test examples that have been
// included with this code.  It is not intended as a general purpose solution which might depend 
// pre-constructed scene files
void GGGSroot::loadData(float Lon, float Lat, float width,float height)
{
	int grid8r, grid8c;  // grid indices
	int m24Lon,m24Lat;   // used for file name construction
	int mg8Lat,mg8Lon;   // used for file name construction
	int Lat_1, Lon_1;
	char fname[60];
	int iLwin,iRwin,iBwin,iTwin;
	FILE *fpin;
	int r,c;
	int lonStep; // used to iterate in latitude

	float leftWin, rightWin; // the viewing window geographic
	float topWin,bottomWin;
	char fnm[100];

	currentViewWid = width;
	focusLon = Lon;
	focusLat = Lat;

	leftWin = Lon - width/2.0f;	
	rightWin = Lon + width/2.0f;
	bottomWin = Lat - height/2.0f;	
	topWin = Lat + height/2.0f;

	cerr << "LEFT RIGHT " << leftWin << " " << rightWin << "\n";
	cerr << "BOTTOM TOP " << bottomWin << " " << topWin << "\n";
	getM8coords(leftWin,bottomWin,mgRowStart,mgColStart);
	getM8coords(rightWin,topWin,mgRowEnd,mgColEnd);
	mgColEnd +=1; mgRowEnd +=1;

	for(c=mgColStart;c < mgColEnd; ++c)
		for(r= mgRowStart; r<mgRowEnd;++r){
			grid8r = r;
			grid8c = c;

			mg8Lon = grid8c*8 - 180; // the file coorner
			mg8Lat = grid8r*8 - 96;

			m24Lon = 24*(int(mg8Lon + 180)/24) - 180;
			m24Lat = 24*(int(mg8Lat + 96)/24) - 96;

			if(mg8Lat == 72) //RULE_72
			{
				grid8c = (grid8c/3)*3;
				m24Lon = 24*(int(mg8Lon + 180)/24) - 180;
				mg8Lon = grid8c*8 - 180;
			}
			if(mg8Lat == 80) //RULE_80
			{
				grid8c = (grid8c/9)*9;
				m24Lon = 24*(int(mg8Lon + 180)/24) - 180;
				mg8Lon = grid8c*8 - 180;
			}
			sprintf(fnm,"M24_%d_%d/BG_8_%d_%d_960.png",m24Lon,m24Lat,mg8Lon,mg8Lat);
			if(globalGrid[grid8r][grid8c]  == NULL)
			{
				// Load a metagrid root file
				globalGrid[grid8r][grid8c] = new metaGridNode(mg8Lat,focusLon,focusLat,double(mg8Lon),double(mg8Lat),0);
				if (!globalGrid[grid8r][grid8c]->insertChild(fnm,focusLat,focusLon,0,0,0,0))
					cerr << "FAILED to open " << fnm << "\n";
				else cerr << "\n\nADD ROOT NODE " << fnm << " "<< grid8r << " " << grid8c << "\n";
				
				// The following code is designed to specifically
				// load the 1 deg test files constructed for this
				// proof of concept prototype.
				// files within this metagrid cell are loaded.
				lonStep = 1;
				iLwin = int(leftWin);
				iRwin = int(rightWin)+1;	
				iBwin = int(bottomWin);			
				iTwin = int(topWin) + 1;
				if(iBwin < mg8Lat)iBwin = mg8Lat; // restrict to this metagrid cell
				if(iLwin < mg8Lon)iLwin = mg8Lon; // restrict to this metagrid cell
				if(iTwin > (mg8Lat + 8)) iTwin = mg8Lat+8;
				if(iRwin >(mg8Lon + 8)) iRwin = mg8Lon + 8;
				
				if(mg8Lat == 72){ //RULE_72  
					// data nodes are 3 deg wide above 72 deg
					if(leftWin < 0.0){
						iLwin = 3*int((leftWin-2.9999)/3.0f);
						iRwin = int(rightWin/3.0)*3;
					}
					else{ 
						iLwin = int((leftWin/3.0f)*3.0f); 
						iRwin = 3*int((rightWin + 2.999999f)/3.0f);
					}
					if(iRwin >= (mg8Lon + 24)) iRwin = mg8Lon+24;
					lonStep = 3;  // increase step between 72 and 80 Lat
					
				}
				if(mg8Lat == 80){  // RULE_80
					// data nodes are 9 deg wide above 80 det
					if(leftWin < 0.0){
						iLwin = 9*int((leftWin-8.9999)/9.0f);
						iRwin = int(rightWin/9.0)*9;
					}
					else{
						iLwin = int((leftWin/9.0f)*9.0f);
						iRwin = 9*int((rightWin + 8.999999f)/9.0f);
					}
					lonStep = 9;
				}
				
				if(iLwin <= mg8Lon) iLwin = mg8Lon; 

				// now run through the 8 deg level files to find all children at this scale
				int childColumn;
			
				for(Lon_1 = iLwin; Lon_1<iRwin; Lon_1 = Lon_1 + lonStep)
				{
					for(Lat_1 = iBwin; Lat_1<iTwin; ++Lat_1)
					{
						calc_1degFname(Lon_1, Lat_1,fname); 
	
						fpin = fopen(fname,"rb"); // check to see if it exists
						if(fpin){ 
							fclose(fpin);
							childColumn = Lon_1-mg8Lon;
							if(Lat_1 >= 72)childColumn = childColumn/3; //RULE_72
							if(Lat_1 >= 80)childColumn = childColumn/3; //RULE_80

							globalGrid[grid8r][grid8c]->insertChild(fname,
							focusLat, focusLon, childColumn, Lat_1 - mg8Lat, 0, 3);
						}
						else cerr << "FAILED TO OPEN " << fname << "\n";		
					}	
				}
				globalGrid[grid8r][grid8c]->pushDown(3);
		}
	}

	metaGridNode *Es,*Ws,*Ss,*Ns,*Ss2,*Ss3;
	int start;

	for(r= mgRowStart; r<=mgRowEnd;++r){
		mg8Lon =c*8 - 180; // the file coorner
		mg8Lat = r*8 - 96;
		start = mgColStart;
		if(mg8Lat == 72)  //RULE_72
		  start = (mgColStart/3)*3;
		if(mg8Lat == 80)  //RULE_80
		  start = (mgColStart/9)*9;
		
		if(mg8Lat <= 80)
			for(c=start;c < mgColEnd; ++c){

				grid8r = r; grid8c = c;	

				Es = Ws = Ss = Ns = NULL;
				if (globalGrid[grid8r][grid8c]!=NULL)
				{
					genSibs(grid8r,grid8c,&Es,&Ws,&Ns,&Ss,&Ss2,&Ss3);		
										
					if(grid8r == 22) // deal with the 80 deg boundary
					{
						globalGrid[grid8r][grid8c]->linkSouthEdge72_80(Ss,Ss2,Ss3);
					}
					globalGrid[grid8r][grid8c]->linkSibs(mg8Lat,Es, Ws, Ns, Ss);			
				}
			}
	}
	for(r= mgRowStart; r<=mgRowEnd;++r){
		mg8Lon =c*8 - 180; // the file coorner
		mg8Lat = r*8 - 96;
		start = mgColStart;
		if(mg8Lat == 72)  //RULE_72
		  start = (mgColStart/3)*3;
		if(mg8Lat == 80)  //RULE_80
		  start = (mgColStart/9)*9;
		
		if(mg8Lat <= 80)
			for(c=start;c < mgColEnd; ++c){

				grid8r = r; grid8c = c;	
				Es = Ws = Ss = Ns = NULL;
				if (globalGrid[grid8r][grid8c]!=NULL)
				{
					//globalGrid[grid8r][grid8c]->printChildren();  // useful for debugging
					globalGrid[grid8r][grid8c]->buildStitchLists();
					//globalGrid[grid8r][grid8c]->printStitchList(15);// useful for debugging

					if(mg8Lat == 80 ||mg8Lat == 80) globalGrid[grid8r][grid8c]->gen_72_80_BorderStrip();
		
				}
			}

	}
}

void GGGSroot::genSibs(int grid8r, int grid8c,
					   metaGridNode **Es,metaGridNode **Ws,metaGridNode **Ns,metaGridNode **Ss,
					   metaGridNode **Ss2,metaGridNode **Ss3) // for 80 and 72
{
	
	if(grid8c > 0)
			*Ws = globalGrid[grid8r][grid8c-1];
	else
			*Ws = globalGrid[grid8r][44];
	*Es = globalGrid[grid8r][grid8c+1];
	*Ns = globalGrid[grid8r+1][grid8c];  
	*Ss = globalGrid[grid8r-1][grid8c];

	if(grid8r == 21){ //RULE_72

		*Es = globalGrid[grid8r][grid8c+3];
		if(grid8c > 2)
			*Ws = globalGrid[grid8r][grid8c-3];
		else 
			*Ws = globalGrid[grid8r][42];
		*Ss2 = globalGrid[grid8r-1][grid8c+1];
		*Ss3 = globalGrid[grid8r-1][grid8c+2];				
	}

	if(grid8r == 22){ //RULE_80

		*Es = globalGrid[grid8r][grid8c+9];
		if(grid8c > 8)
				*Ws = globalGrid[grid8r][grid8c-9];
		else 
			*Ws = globalGrid[grid8r][36];
		*Ss2 = globalGrid[grid8r-1][grid8c+3];
		*Ss3 = globalGrid[grid8r-1][grid8c+6];
	}			
}

void GGGSroot::getM8coords(float refLon, float refLat, int &Row, int &Col)
{
	Row = int(refLat + 96.0f)/8;
	Col = int(refLon + 180.0f)/8;
}

void GGGSroot::loadColorMaps()
{
	colormap = new cmap();
	colormap->load("ColorfulRB_Light.txt",true,0);
	colormap->load("Grey2.txt",true,1);
	colormap->mkTex2Dcmap();

}

void GGGSroot::draw(bool lores)
{
	int r,c;
	int startc;
	int mg8Lat;

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, colormap->tex2dNames[1]);

	for(r= mgRowStart; r<=mgRowEnd;++r){
		mg8Lat = r*8 - 96;
		startc = mgColStart;
		if(mg8Lat == 72)  //RULE_72
			startc = (mgColStart/3)*3;
		if(mg8Lat == 80)  //RULE_80
			startc = (mgColStart/9)*9;
		for(c=startc;c <= mgColEnd; ++c){				
			if(globalGrid[r][c]!=NULL){

				//glDisable(GL_LIGHTING); // Use these lines to draw the area
				//globalGrid[r][c]->drawTreeAreas(0);
				globalGrid[r][c]->drawTree(0,lores);
				if(mg8Lat == 80)globalGrid[r][c]->drawTriangleList();
			}
		}
	}
	glDisable(GL_TEXTURE_2D);
}

void GGGSroot::calc_1degFname(int fLon, int fLat, char *fname)
{
	int m24Lon,m24Lat, m8Lon, m8Lat;

	m24Lon = 24*((fLon + 180)/24) - 180;
	m24Lat = 24*((fLat + 96)/24) - 96;

	m8Lon = 8*((fLon + 180)/8) - 180;
	m8Lat = 8*((fLat + 96)/8) - 96;

	if(fLat >=72) m8Lon = m24Lon; //RULE_72 to accomodate wide data files
	if(fLat >=80)
	{
		m8Lon = 72*((fLon + 180)/72) - 180; //RULE_80 to accomodate wide data files
		m24Lon = 72*((fLon + 180)/72) - 180;
	}

	sprintf(fname,"M24_%d_%d/M8_%d_%d/BG_1_%d_%d_480.png",m24Lon,m24Lat,m8Lon,m8Lat,fLon,fLat);
}
