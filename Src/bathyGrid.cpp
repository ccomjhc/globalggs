/*-----------------------------------------------------------------------------------------
Copyright(c) 2019, 2019 University of New Hampshire, Center for Coastal and Ocean Mapping,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met :
*Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and / or other materials provided with the distribution.
* Neither the name of the <organization> nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************************/
////////////////////////////////////////////////////////////////
//
// BathyGrid.cpp: A regular grid of bathymetric depths includes rendering methods
// For use within the framework of the Global Geographic Grid System (GGGS).
//
//////////////////////////////////////////////////////////////////////

#include <math.h>
#include <iostream>
#include "bathyGrid.h"
#include <GL/glut.h>
#include "readPNG.h"
#include "cmap.h"

#define PI 3.1415926536

#define EARTH_RAD 6371000.0

#define X 0
#define Y 1
#define Z 2

using namespace std;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


/*--------------------------------------------------------------------------*/
bathyGrid::bathyGrid(double LLon,double LLat,double ht, float widMult, int nID)
{
	nX = 0;
	nY = 0;	

	LL_Lon = LLon;  LL_Lat = LLat; 
	nodeID = nID;

	widthMultiplier = widMult;
	
	height = ht;
	width = height*widMult;

}

/*--------------------------------------------------------------------------*/
bathyGrid::~bathyGrid()
{
	int i;
	
	if (nX > 0 && nY > 0)
	{
		
		for (i = 0; i < nY; i++)
		{
			delete [] norms_array[i];
		}
		delete norms_array;
		
		for (i = 0; i < nY; i++)
		{
			delete []  dem[i];
		}
		delete dem;	

		for (i = 0; i < nY; i++)
		{
			delete []  attrib[i];
		}
		delete attrib;
	}
}


// The dem rendering method
// Depth coding uses two colormaps, based on the attribute values.
void bathyGrid::DrawDem(bool lores)
{
	// Note also, that the vertical scale here only keeps the dem inside the clipping plane
	// It is otherwise irrelevant given an orthographic projectio

	int r, c, skip;
	float yCell;
	vNorms *row1;
	vNorms *row2;
	float *h1;
	float *h2;
	float v;//,x,y;
	float cmv;

	glColor3f(1.0, 1.0, 1.0);

	skip = 1;
	if (lores)skip = 2;

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	for (r = 0; r < nY - skip; r = r + skip)
	{
		row1 = norms_array[r];
		row2 = norms_array[r + skip];

		h1 = dem[r];
		h2 = dem[r + skip];

		glBegin(GL_TRIANGLE_STRIP);
		for (c = 0; c < nX; c = c + skip)
		{
			v = -h1[c] * TEX_SCALE;

			cmv = float(attrib[r][c]);

			glTexCoord2f(cmv, v); // surface colors are defined by a 2D texture map
			glNormal3sv(row1[c]);
			glVertex3f(mapX[r][c], mapY[r][c], h1[c] * VERT_SCALE);

			v = -h2[c] * TEX_SCALE;
			cmv = float(attrib[r + skip][c]);
			glTexCoord2f(cmv, v);
			glNormal3sv(row2[c]);
			glVertex3f(mapX[r + skip][c], mapY[r + skip][c], h2[c] * VERT_SCALE);
		}
		glEnd();
	}
}


bool bathyGrid::loadPNGbathy(char *fname, double focLat, float rLon, float rLat, int cmap)
{
	int w, h, chan, rowBytes;
	int r, c, ii, rr, gg;
	FILE *fpin;

	int count = 0;
	unsigned char *imageX;

	float min, max;

	cerr << "READ PNG FILE NAME " << fname << "\n";

	relLon = rLon; relLat = rLat;

	pngread = new readPNG();
	fpin = fopen(fname, "rb");

	if (!fpin) return false;

	pngread->readpng_init(fpin, &w, &h);
	imageX = new unsigned char[w*h * 3];

	nX = w; nY = h;

	int maxRR = -10;
	allocateMem();  //Note only do once

	pngread->readpng_get_image(1.0, &chan, &rowBytes, imageX);

	for (r = 0; r<nY; ++r) {
		for (c = 0; c < nX; ++c) {
			ii = r*nX * 3 + c * 3;
			rr = imageX[ii];
			gg = imageX[ii + 1];
			dem[r][c] = -float(rr * 256 + gg)*0.2f;
			attrib[r][c] = cmap;
		}
	}

	cellSize = float(height / double(nY));

	computeXYfromLatLon(focLat, rLon, rLat, false);

	CalculateNormals2();

	return true;
}


// Reprojects from Lat Lon coordinates to creates a an orthographic projection 
// on a plane tangential to the earth at the specified focus point
void bathyGrid::computeXYfromLatLon(double focusLat, float relLon, float relLat, bool print)
{
	int r, c;
	double Lat, Lon;  // the actual lat, lon of depths
	double degToRad;
	double rad; // the radius of the unit sphere at the specified latitude
	double x, y, yy, z;
	double cosFc_Lat, sinFc_Lat;
	float lonCellSize;

	degToRad = 3.14159265359 / 180.0;

	lonCellSize = widthMultiplier*cellSize*float(nY) / float(nX);

	cosFc_Lat = cos(focusLat*degToRad);
	sinFc_Lat = sin(focusLat*degToRad);


	for (r = 0; r<nY; ++r) {
		Lat = LL_Lat + cellSize*r;
		rad = cos(Lat*degToRad);
		y = sin(Lat*degToRad);
		for (c = 0; c < nX; ++c) {

			Lon = relLon + lonCellSize*c;
			x = sin(Lon*degToRad)*rad;
			z = -cos(Lon*degToRad)*rad;
			// now rotate to get new Y;
			yy = y*cosFc_Lat + z*sinFc_Lat;
			mapX[r][c] = x / degToRad; // result in degrees
			mapY[r][c] = yy / degToRad;
		}
	}

	if (print) { // for debugging
		FILE *fo;
		fo = fopen("coords.csv", "w");

		for (r = 0; r<nY; r = r + 50) {
			for (c = 0; c < nX; c = c + 50) {
				fprintf(fo, "%f, %f \n", mapX[r][c], mapY[r][c]);
			}
		}
		fclose(fo);
	}
}




// Allocate 2D arrays for DEM, reprojected Lat Lon, normals and attributes
int bathyGrid:: allocateMem()
{
	// There are four structures
	// 1) The DEM
	// 2) X,Y positions for reprojected Lat, Lon
	// 3) An array of surface normals used in rendering
	// 4) An attribute array
	
	int r,c;
	
	dem = new float*[nY];
	mapX = new float*[nY];
	mapY = new float*[nY];
	attrib = new unsigned char*[nY];
	for (r = 0; r < nY; r++){
		dem[r] = new float[nX];
		mapX[r] = new float[nX];
		mapY[r] = new float[nX];
		attrib[r] = new unsigned char[nX];
	}

	norms_array = new vNorms*[nY];
	for (r = 0; r < nY; r++){
		norms_array[r] = new vNorms[nX];
	}

	for (r = 0; r < nY; r++)
		for(c=0;c<nX; ++c) attrib[r][c] = 1;  // grey is the default

	return 0;
}

// Draw area as a projected rectangle.  Used for illustrations
void bathyGrid::DrawArea(float relLon,float relLat)
{

	int c;
	glBegin(GL_TRIANGLE_STRIP);
	for(c=1;c<nX-2; c=c+2){
		glVertex2f(mapX[3][c],mapY[3][c]);
		glVertex2f(mapX[nY-2][c],mapY[nY-2][c]);
	}
	glEnd();
	glColor3f(0.0,0.0,0.0);
	glLineWidth(3.0f);
	glBegin(GL_LINES);
		glVertex2f(mapX[0][0],mapY[0][0]);
		glVertex2f(mapX[nY-1][0],mapY[nY-1][0]);
	glEnd();
	glBegin(GL_LINE_STRIP);
	for(c=0;c<nX;++c){
		glVertex2f(mapX[0][c],mapY[0][c]);
	}
	glEnd();
		
}

// used in filling the corners at the intersection of grids
void bathyGrid::getSWcornerVal(p3d &corner)
{
	corner.pt[0] = mapX[0][0];
	corner.pt[1] = mapY[0][0];
	corner.pt[2] = dem[0][0];
}

// Draw the stitch lists to fill the gaps between dems.
void bathyGrid::DrawEastEdge2(stitchList2 *east, bool lores)
{
	int r, c, lc, Llen, Li;  // Li is the right index
	int Ec; // east counter
	float x1, x2, y1, y2;
	float v1, v2, cv;
	int ratio;
	float yStart;
	float cmv;
	int count;

	bathyGrid *bg;

	int gap; gap = 1;
	if (lores) gap = 2;

	if (east == NULL)return;

	glBegin(GL_TRIANGLE_STRIP);
	for (lc = 0; lc<east->count; ++lc) // go through all the strips
	{
		bg = east->dtmSList[lc];

		Llen = east->demStarts[lc + 1] - east->demStarts[lc];
		Ec = east->npts[lc];

		ratio = Ec / Llen;
		if (ratio < 1) return;

		c = nX - gap;

		for (r = 0; r < Ec; r = r + gap)
		{

			Li = (r) / ratio + east->demStarts[lc];

			x1 = mapX[Li][c];
			x2 = bg->mapX[r][0];

			y1 = mapY[Li][c];
			y2 = bg->mapY[r][0];

			v1 = dem[Li][c];
			v2 = bg->dem[r][0];

			cmv = attrib[Li][c];

			cv = -v1*TEX_SCALE;
			glTexCoord2f(cmv, cv);
			glNormal3sv(norms_array[Li][c]);

			glVertex3f(x1, y1, v1*VERT_SCALE);
			cv = -v2*TEX_SCALE;
			glTexCoord2f(cmv, cv);
			glVertex3f(x2, y2, v2*VERT_SCALE);
		}
	}
	glEnd();
}



void bathyGrid::DrawNorthEdge2(stitchList2 *north, bool lores)
{
	int r, c, lc, Llen, Li;  
	int Nc; // north counter
	float x1, x2, y1, y2;
	float v1, v2, cv;
	int ratio;

	float cmv;
	int count;

	bathyGrid *bg;
	int gap; gap = 1;
	if (lores) gap = 2;

	if (north == NULL)return;

	glBegin(GL_TRIANGLE_STRIP);
	for (lc = 0; lc<north->count; ++lc) // go through all the strips
	{
		bg = north->dtmSList[lc];
		Llen = north->demStarts[lc + 1] - north->demStarts[lc];
		Nc = north->npts[lc];
		ratio = Nc / Llen;

	if (ratio < 1) return;

		r = nY - gap - 1;

		for (c = 0; c < Nc; c= c + gap)
		{
			Li = c/ ratio + north->demStarts[lc]; 

			x1 = mapX[r][Li];
			x2 = bg->mapX[0][c];
	
			y1 = mapY[r][Li];
			y2 = bg->mapY[0][c];

			v1 = dem[r][Li];
			v2 = bg->dem[0][c];
		
			cmv = attrib[r][Li]; 

			cv = -v1*TEX_SCALE;
			glTexCoord2f(cmv, cv);
			glNormal3sv(norms_array[r][Li]);

			glVertex3f(x1, y1, v1*VERT_SCALE);
			cv = -v2*TEX_SCALE;
			glTexCoord2f(cmv, cv);
			glVertex3f(x2, y2, v2*VERT_SCALE);
	
		}	
	}
	glEnd();
}

void bathyGrid::DrawSouthEdge2(stitchList2 *north, bool lores)
{
	int r, c, lc, Llen, Li;  // Li is the right index
	int Nc, nRow; // south counter
	float x1, x2, y1, y2;
	float v1, v2, cv;
	int ratio;
	float cellHtR, cellHtL;
	float cmv;
	int count;

	bathyGrid *bg;
	int gap; gap = 1;
	if (lores) gap = 2;
	cellHtL = float(width / double(nY));

	if (north == NULL)return;

	glBegin(GL_TRIANGLE_STRIP);
	for (lc = 0; lc<north->count; ++lc) // go through all the strips
	{
		bg = north->dtmSList[lc];

		Llen = north->demStarts[lc + 1] - north->demStarts[lc];
		Nc = north->npts[lc];
		ratio = Nc / Llen;

		nRow = bg->nY - gap - 1; // rhColumn

		if (ratio < 1) return;

		r = 0;

		for (c = 0; c < Nc; c = c + gap)// was nY-1
		{

			Li = (c) / ratio + north->demStarts[lc]; // left index

			x1 = mapX[r][Li];
			x2 = bg->mapX[nRow][c];

			y1 = mapY[r][Li];
			y2 = bg->mapY[nRow][c];

			v1 = dem[r][Li];
			v2 = bg->dem[nRow][c];

			cmv = attrib[r][Li]; // make more efficient

			cv = -v1*TEX_SCALE;
			glTexCoord2f(cmv, cv);
			glNormal3sv(norms_array[r][Li]);

			glVertex3f(x1, y1, v1*VERT_SCALE);
			cv = -v2*TEX_SCALE;
			glTexCoord2f(cmv, cv);
			glVertex3f(x2, y2, v2*VERT_SCALE);

		}
	}
	glEnd();
}

void bathyGrid::DrawWestEdge2(stitchList2 *west, bool lores)
{
	int r, c, lc, Llen, Li;  // Li is the right index
	int Ec; // west counter
	float x1, x2, y1, y2;
	float v1, v2, cv;
	int ratio;

	float cmv;
	int count, rhC;

	bathyGrid *bg;

	int gap; gap = 1;
	if (lores) gap = 2;
	if (west == NULL)return;

	glBegin(GL_TRIANGLE_STRIP);
	for (lc = 0; lc<west->count; ++lc) // go through all the strips
	{
		bg = west->dtmSList[lc];
		rhC = bg->nX - gap - 1; // rhColumn

		Llen = west->demStarts[lc + 1] - west->demStarts[lc];
		Ec = west->npts[lc];

		ratio = Ec / Llen;
		if (ratio < 1) return; 

		c = 0;

		for (r = 0; r < Ec; r = r + gap)
		{

			Li = (r) / ratio + west->demStarts[lc]; // left index

			x1 = mapX[Li][c];
			x2 = bg->mapX[r][rhC];

			y1 = mapY[Li][c];
			y2 = bg->mapY[r][rhC];

			v1 = dem[Li][c];
			v2 = bg->dem[r][rhC];

			cmv = attrib[Li][c];

			cv = -v1*TEX_SCALE;
			glTexCoord2f(cmv, cv);
			glNormal3sv(norms_array[Li][c]);

			glVertex3f(x1, y1, v1*VERT_SCALE);
			cv = -v2*TEX_SCALE;
			glTexCoord2f(cmv, cv);
			glVertex3f(x2, y2, v2*VERT_SCALE);
		}
	}
	glEnd();
}


void bathyGrid::copySouthEdgeV(vector<p3d> &southPointList,int &southPointCount)
{
	int r,c;
	p3d vt;
	r=0;
	for(c=0;c<nX;++c){
		vt.pt[0] = mapX[r][c];
		vt.pt[1] = mapY[r][c];		
		vt.pt[2] = dem[r][c]*VERT_SCALE;
		
		vt.pn[0] = norms_array[r][c][0];
		vt.pn[1] = norms_array[r][c][1];		
		vt.pn[2] = norms_array[r][c][2];

		vt.attrib = attrib[r][c];
		southPointList.push_back(vt);
		++southPointCount;
	}
}

void bathyGrid::copyNorthEdgeV(vector<p3d> &northPointList,int &northPointCount)
{
	int r,c;

	p3d vt;
	r=nY-2;
	for(c=0;c<nX;++c){
		vt.pt[0] = mapX[r][c];
		vt.pt[1] = mapY[r][c];		
		vt.pt[2] = dem[r][c]*VERT_SCALE;

		vt.pn[0] = norms_array[r][c][0];
		vt.pn[1] = norms_array[r][c][1];		
		vt.pn[2] = norms_array[r][c][2];

		vt.attrib = attrib[r][c];
		northPointList.push_back(vt);
		++northPointCount;
	}

}


/*--------------------------------------------------------------------------*/
bool bathyGrid::addDTMquadrant(bathyGrid *ddtm,int child)
{
	int r,c,i,coff,roff;
	nX = ddtm->nX/2;
	nY = ddtm->nY/2;
	allocateMem();

	coff = roff = 0;
	if(child%2 == 1)coff = nX;
	if(child/2 == 1)roff = nY;

	for(r = 0; r < nY; ++r)
	{	
		for(c = 0; c < nX ; ++c)
		{	
			dem[r][c] = ddtm->dem[r+roff][c+coff];
			attrib[r][c] = ddtm->attrib[r+roff][c+coff]; //XXX DGB
			mapX[r][c] = ddtm->mapX[r+roff][c+coff];
			mapY[r][c] = ddtm->mapY[r+roff][c+coff];
			for(i=0;i<3;++i) norms_array[r][c][i] = ddtm->norms_array[r+roff][c+coff][i];
			
		}
	}

	return true;

}

bool bathyGrid::dtmQuadrantMerge(bathyGrid *ddtm,int child)
{
	int r,c,i,coff,roff;
	int ratioX,ratioY;
	int inY,inX;
	inY = ddtm->nY/2;
	inX = ddtm->nX/2;
	roff = coff = 0;
	if(child%2 == 1)coff = inX;
	if(child/2 == 1)roff = inY;

	// need offset for the other children

	ratioY = nY/inY;
	ratioX = nX/inX;
	//cerr << "MERGE RATIO " << ratioX << "\n";

	for(r = 0; r < nY; ++r)
	{	
		for(c = 0; c < nX ; ++c)
		{	
			if(dem[r][c] < -9950.0f) // HACK was - 10000.0 should be 12000
			{
				dem[r][c] = ddtm->dem[r/ratioY+roff][c/ratioX+coff];
				attrib[r][c] = ddtm->attrib[r/ratioY+roff][c/ratioX+coff];
				//dem[r][c] = ddtm->dem[r/ratio+roff][(c+coff)/ratio];
			    for(i=0;i<3;++i) norms_array[r][c][i] = ddtm->norms_array[r/ratioY+roff][c/ratioX+coff][i];
			}

		}
	}
	return true;
}


void bathyGrid::CalculateNormals2()
{
	float v1, v2, v3;
	int i, j;
	float dhx, dhy, dhz, len;

	float dx, dy, dxy2;

	dx = mapX[nY / 2][nX / 2] - mapX[nY / 2][nX / 2 - 1];
	dy = mapY[nY / 2][nX / 2] - mapY[nY / 2 - 1][nX / 2];

	dxy2 = dx*dx + dy*dy;
	// use forward differences to calculate normals

	for (i = 0; i < nY - 1; i++)
	{
		for (j = 0; j < nX - 1; j++)
		{
			v1 = dem[i][j] * SHADE_SCALE;
			v2 = dem[i + 1][j] * SHADE_SCALE;
			v3 = dem[i][j + 1] * SHADE_SCALE;

			dhx = (v1 - v3) / dx;
			dhz = (v1 - v2) / dy;
			// we assume the grid gap is 1.0

			len = (float)sqrt(dhx*dhx + dhz*dhz + 1.0);

			dhx /= len;
			dhz /= len;
			dhy = 1.0 / len;

			norms_array[i][j][X] = -int(dhx*32000.0f);
			norms_array[i][j][Y] = -int(dhy*32000.0f);
			norms_array[i][j][Z] = -int(dhz*32000.0f);
		}
		norms_array[i][nX - 1][X] = norms_array[i][nX - 2][X];
		norms_array[i][nX - 1][Y] = norms_array[i][nX - 2][Y];
		norms_array[i][nX - 1][Z] = norms_array[i][nX - 2][Z];
	}
	for (i = 0; i < nX; i++) // make the last row the same
	{

		norms_array[(nY - 1)][i][X] = norms_array[(nY - 2)][i][X];
		norms_array[(nY - 1)][i][Y] = norms_array[(nY - 2)][i][Y];
		norms_array[(nY - 1)][i][Z] = norms_array[(nY - 2)][i][Z];
	}
}

