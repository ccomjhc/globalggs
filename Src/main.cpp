/*-----------------------------------------------------------------------------------------
Copyright(c) 2019, 2019 University of New Hampshire, Center for Coastal and Ocean Mapping,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met :
*Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and / or other materials provided with the distribution.
* Neither the name of the <organization> nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************************/
/////////////////////////////////////////////////////////////////////////////////
// A test driver for an implementation of the Global Geographic Grid System (GGGS)
//
/////////////////////////////////////////////////////////////////////////////////
#include <GL/glut.h>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include "GGGSroot.h"

int globalCount; // Used to count the number of nodes in the set of quad trees.

double setTime;
using namespace std ;

float winWid,winHeight;
float transX,transY;
float scale;

GGGSroot *root;

bool loRes;


void redraw( void )
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_NORMALIZE);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glPushMatrix();
		glTranslatef(0.0f,1.0f,0.0f);
		glRotatef(-55.0f,1.0f,0.0f,0.0f);
		glTranslatef(transX,transY,0.0f);
		glScalef(scale,scale,scale);
		root->draw(loRes);
	glPopMatrix();

	glutSwapBuffers();
}


void arrowKeys(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP: transY -= 1.0f;
		break;
	case GLUT_KEY_DOWN: transY += 1.0f;
		break;
	case GLUT_KEY_RIGHT: transX -= 1.0f;
		break;
	case GLUT_KEY_LEFT: transX += 1.0f;
		break;
	}

	redraw();
}

void keyboard(unsigned char key, int x, int y)
// x and y givethe mouse pos
{
	switch (key)
	{

	case 'h': loRes = false;  // hitting this key causes a high resolution view to be drawn
		// complete with ties between regions.
		break;
	case '.': scale = scale*1.25f;
		break;
	case ',': scale = scale / 1.25f;
		break;
	}
	redraw();
	loRes = true;
}

void setupGuiGraphics()
{
	winWid = 1350.0;
	winHeight = 750.0;
	transX = transY = 0.0f;
	scale = 2.0f;

	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutCreateWindow("Global Geographic Grid System");
	glutPositionWindow(200, 50);
	glutReshapeWindow(int(winWid), int(winHeight));

	glClearColor(0.0, 0.0, 0.0, 1.0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glFrustum(-5.4f, 5.4f, -3.0f, 3.0f, 50.0, 500.0);
	glTranslatef(0.0, 0.0, -60.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	float light_position[] = {-10.0,-15.0,10.0,0.0};

	glLightfv(GL_LIGHT0,GL_POSITION, light_position);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_NORMALIZE);
	glEnable(GL_DEPTH_TEST);
	glutKeyboardFunc(keyboard);
	glutDisplayFunc(redraw);
	//glutIdleFunc(redraw);
	root->loadColorMaps();
}



int main(int argc, char *argv[])
{
	cerr << "hello Bathy World\n";
	loRes = true;
	globalCount = 1;

	root = new GGGSroot();


	// THREE Examples
	// These define the centers and the extent of a set of test example
	// Data for these is provided
	// Inputs are Lon, Lat, Lon extent, Lat extent

	root->loadData(-160.2f, 81.1f, 12.2f, 4.2f); // 
	//root->loadData(-170.2f, 20.1f, 8.2f, 5.2f);  //
	//root->loadData(-162.8f, 18.5f, 1.2f, 0.5f); // 

	glutInit(&argc, argv);

	setupGuiGraphics();

	glutMainLoop();

	return 0;
}






