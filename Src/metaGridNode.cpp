/*-----------------------------------------------------------------------------------------
Copyright(c) 2019, 2019 University of New Hampshire, Center for Coastal and Ocean Mapping,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met :
*Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and / or other materials provided with the distribution.
* Neither the name of the <organization> nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************************/

//////////////////////////////////////////////////////////////////////
// metaGridNode.cpp: implementation of the metaGridNode of the Global Geographic Grid System (GGGS).
// A quad tree subdivision framework starting with 8x8 deg.
// Part of an implementation of the Global Geographic Grid System (GGGS).
//
//////////////////////////////////////////////////////////////////////


#include <stdlib.h>
#include <iostream>
#include <GL/glut.h>
#include <math.h>
#include "metaGridNode.h"
#include "bathyGrid.h"
#include "cmap.h"

extern int globalCount;

using namespace std ;

metaGridNode :: metaGridNode(int mg8Lat, double focusLon, double focusLat,
							 double LLon, double LLat,int depth)
{
	int i, ht;

	for(i=0;i<4;++i) children[i] = NULL;
	Nsib = Ssib = Esib = Wsib = NULL;
	Ssib2 = Ssib3 = NULL;

	frame8Lat = mg8Lat;

	estitch = NULL; // new
	wstitch = NULL;
	nstitch = NULL;
	sstitch = NULL;
		
	dtm = NULL;
	
	nodeID = globalCount;
	localLon = focusLon;
	localLat = focusLat; 
	LL_Lon = LLon;
	LL_Lat = LLat;

	relLon = float(LLon-focusLon);
	relLat = float(LLat-focusLat);  //XXX Rel

	hasChildren = false;
	thisDepth = depth;

	lonMultiplier = 1.0f;
	if(LLat >= 72.0)lonMultiplier = 3.0f; //RULE_72
	if(LLat >= 80.0)lonMultiplier = 9.0f; //RULE_80

	// set the vertical node extent in degrees
	ht = 1;
	for (i = 0; i<depth; ++i) ht = ht * 2;
	nodeHeight = 8.0/double(ht);

	nodeWidth = lonMultiplier*nodeHeight; // the node width in degrees

	++globalCount;

}

// This method recursively builds the tree (unless it already exists) until it gets to the appropriate spatial location and depth
// then it inserts the data grid at that location
bool metaGridNode :: insertChild(char *fname, double focusLat,double focusLon, 
		int childC, int childR,int depth,int Tdepth)// we do not need m8s
{
	int i,height;
	double LLat,LLon; // for children
	height = Tdepth - depth;

	int cm;
	if(depth == 0) cm = 1;
	else cm = 0;

	if(height == 0)// insert file here
	{
		dtm = new bathyGrid(LL_Lon,LL_Lat,nodeHeight,lonMultiplier, nodeID);
		if(!dtm->loadPNGbathy(fname,focusLat,relLon,relLat,cm)) return false;

		return true;
	}

	// othewise recurse down
	int cc,cr;  // child row, column
	int mod;
	mod =8;
	for(i=0;i<depth;++i) mod = mod/2;
	cr = (childR%mod)/(mod/2);
	cc = (childC%mod)/(mod/2);
	
	LLat = LL_Lat + cr*nodeHeight*0.5;

	LLon = LL_Lon + cc*nodeWidth*0.5;

	if(children[cr*2 + cc] == NULL)	
		children[cr*2 + cc] = new  metaGridNode(frame8Lat,focusLon,focusLat,LLon,LLat,depth+1);

	hasChildren = true; 
	return children[cr*2 + cc]->insertChild(fname,  focusLat,focusLon, 
		 childC,  childR,depth+1,Tdepth);

}

// The rendering method. Recursively traverses the metaGrid tree and draws dems where they exist
void metaGridNode::drawTree(int depth, bool lores)
{
	int i;

	if (dtm != NULL)
	{
		if (!hasChildren) // draw leaf nodes only
		{
			dtm->DrawDem(lores);
			if (!lores)
			{
				dtm->DrawEastEdge2(estitch, lores);
				dtm->DrawWestEdge2(wstitch, lores);
				dtm->DrawNorthEdge2(nstitch,lores);
				dtm->DrawSouthEdge2(sstitch, lores);
			}
		}

	}
	for (i = 0; i<4; ++i)
	{
		if (children[i] != NULL)
			children[i]->drawTree(depth + 1, lores);
	}

}

// draws tree areas as colored rectangles based on depth.
void metaGridNode::drawTreeAreas(int depth)
{
	int i;

	if (depth == 0)glColor3f(1.0, 1.0, 0.0);
	if (depth == 1)glColor3f(1.0, 0.0, 1.0);
	if (depth == 2)glColor3f(0.0, 1.0, 1.0);
	if (depth == 3)glColor3f(0.0, 1.0, 0.0);
	if (depth == 4)glColor3f(1.0, 0.0, 1.0);

	if (!hasChildren) {
		dtm->DrawArea(relLon, relLat);

		return;
	}

	for (i = 0; i<4; ++i)
	{
		if (children[i] != NULL)
			children[i]->drawTreeAreas(depth + 1);
	}

}
	
// link to neighbors (sibs and cousins)
// inputs are from parents who know who my neighbors are
void metaGridNode::linkSibs(int mg8Lat, metaGridNode *sE, metaGridNode *sW,
	metaGridNode *sN, metaGridNode *sS)
{


	metaGridNode *sdW, *sdE, *sdN, *sdS;

	Esib = sE;
	Wsib = sW;
	Nsib = sN;
	Ssib = sS;

	sdW = sdE = sdN = sdS = NULL;

	if (children[0] != NULL) {
		if (Wsib != NULL)
			sdW = Wsib->children[1]; // right
		if (Ssib != NULL)
			sdS = Ssib->children[2];

		sdE = children[1];
		sdN = children[2];
		children[0]->linkSibs(frame8Lat, sdE, sdW, sdN, sdS);
	}


	if (children[1] != NULL) {
		sdW = sdE = sdN = sdS = NULL;
		if (Esib != NULL)
			sdE = Esib->children[0]; // right
		if (Ssib != NULL)
			sdS = Ssib->children[3];
		sdW = children[0];
		sdN = children[3];
		children[1]->linkSibs(frame8Lat, sdE, sdW, sdN, sdS);
	}


	if (children[2] != NULL) {
		sdW = sdE = sdN = sdS = NULL;
		if (Wsib != NULL)
			sdW = Wsib->children[3]; // right
		if (Nsib != NULL)
			sdN = Nsib->children[0];
		sdE = children[3];
		sdS = children[0];
		children[2]->linkSibs(frame8Lat, sdE, sdW, sdN, sdS);
	}


	if (children[3] != NULL) {
		sdW = sdE = sdN = sdS = NULL;
		if (Esib != NULL)
			sdE = Esib->children[2]; // right
		if (Nsib != NULL)
			sdN = Nsib->children[1];
		sdW = children[2];
		sdS = children[1];
		children[3]->linkSibs(frame8Lat, sdE, sdW, sdN, sdS);
	}
}

// This is used for a particular kind of multibeam data (from GMRT) This data has gaps between swaths
// Pushdown fills these gaps with lower resolution data.
void metaGridNode::pushDown(int depth)
{
	int i;

	double offLat, offLon;
	// purpose split dtm into 4 and push down for fill around hi-res dtms
	if (!hasChildren) return;
	// otherwise split dem into 4 and send parts to children
	for (i = 0; i<4; ++i)
	{
		offLat = offLon = 0.0;
		if (i % 2 == 1) offLon = nodeWidth*0.5;
		if (i / 2 == 1) offLat = nodeHeight*0.5;

		if (children[i] == NULL)
		{
			children[i] = new  metaGridNode(frame8Lat, localLon, localLat, LL_Lon + offLon, LL_Lat + offLat, depth + 1);
		}
		children[i]->insertDtmQuadrant(dtm, i);
	}
}

//Splits a dem into four parts and pushes them lower in the tree
// Purpose is to fill around higher resolution data
void metaGridNode::insertDtmQuadrant(bathyGrid *ddtm, int child)
{
	if (dtm == NULL)
	{
		dtm = new bathyGrid(LL_Lon, LL_Lat, nodeWidth, lonMultiplier, nodeID);
		dtm->addDTMquadrant(ddtm, child);

	}
	else
	{
		dtm->dtmQuadrantMerge(ddtm, child);
	}
	if (hasChildren)pushDown(thisDepth);

}

// Builds a data structure for use in stitching together adjacent dems.
void metaGridNode::buildStitchLists()
{
	int i;
	
	if(!hasChildren){
		
		if(Esib != NULL){

			estitch = new bathyGrid::stitchList2;
			estitch->count = 0; // not needed
			Esib->westEdge(estitch, dtm->nY, 0);
			estitch->demStarts[estitch->count] = dtm->nY;
		}

		if(Wsib != NULL){
			if(Wsib->hasChildren||(Wsib->dtm->nY > dtm->nY)){ // build a stitch list

				wstitch = new bathyGrid::stitchList2;
				wstitch->count = 0;
				Wsib->eastEdge(wstitch, dtm->nY, 0); 
				wstitch->demStarts[wstitch->count] = dtm->nY;
			}
		}	

		if(Nsib != NULL){
			// check 80 border  //RULE_80  //RULE_72
			if((fabs(Nsib->LL_Lat - 80.0) > 0.00001)  && (fabs(Nsib->LL_Lat - 72.0) > 0.00001))
			{
				nstitch = new bathyGrid::stitchList2;
				nstitch->count = 0;
				Nsib->southEdge(nstitch, dtm->nX, 0);
				nstitch->demStarts[nstitch->count] = dtm->nX;
			}	
			
		}
					
		if(Ssib != NULL){
			if(Ssib->hasChildren||(Ssib->dtm->nX > dtm->nX)){ // build a stitch list	
				if(thisDepth>0 &&  fabs(LL_Lat - 80.0)> 0.00001 && fabs(LL_Lat - 72.0)> 0.00001)
				{	
					sstitch = new bathyGrid::stitchList2;
					sstitch->count = 0;
					Ssib->northEdge(sstitch, dtm->nX, 0); 
					sstitch->demStarts[sstitch->count] = dtm->nX;
				}
			}		
		}

		if((Esib != NULL)&&(Nsib != NULL))
		{
			// get the SW corner
			//cerr << "CORNER  " << nodeID << "\n";
			if(Esib->Nsib != NULL)
				Esib->Nsib->getSWcorner(swCorner);

		}
		return;
	}

	
	for(i=0;i<4;++i)  // now recurse
	{
		if(children[i] !=NULL)
			children[i]->buildStitchLists();
	}
}

// Used by  buildStitchLists to recursively find all neighboring nodes along a 
// particular edge
void metaGridNode::westEdge(bathyGrid::stitchList2 *es, int edgeSize, int edgeStart) // version with no dups
{
	int ct;
	if (!hasChildren)
	{
		ct = es->count;
		es->demStarts[ct] = edgeStart;
		es->npts[ct] = dtm->nY;  // HACK
		es->dtmSList[ct] = dtm;
		es->count = ct + 1;
		return;
	}
	if (children[0] != NULL)
		children[0]->westEdge(es, edgeSize / 2, edgeStart);
	if (children[2] != NULL)
		children[2]->westEdge(es, edgeSize / 2, edgeStart + edgeSize / 2);
}

void metaGridNode::eastEdge(bathyGrid::stitchList2 *ws, int edgeSize, int edgeStart) // version with no dups
{
	int ct;
	if (!hasChildren)
	{
		ct = ws->count;
		ws->demStarts[ct] = edgeStart;
		ws->npts[ct] = dtm->nY;  // HACK
		ws->dtmSList[ct] = dtm;
		ws->count = ct + 1;
		return;
	}
	if (children[1] != NULL)
		children[1]->eastEdge(ws, edgeSize / 2, edgeStart);
	if (children[3] != NULL)
		children[3]->eastEdge(ws, edgeSize / 2, edgeStart + edgeSize / 2);
}

void metaGridNode::southEdge(bathyGrid::stitchList2 *ss, int edgeSize, int edgeStart) 
{
	int ct;

	if (!hasChildren)
	{	
		ct = ss->count;
		ss->demStarts[ct] = edgeStart;
		ss->npts[ct] = dtm->nX;  // HACK
		ss->dtmSList[ct] = dtm;
		ss->count = ct + 1;
		return;
	}
	if (children[0] != NULL)
		children[0]->southEdge(ss, edgeSize / 2, edgeStart);
	if (children[1] != NULL)
		children[1]->southEdge(ss, edgeSize / 2, edgeStart + edgeSize / 2);
}

void metaGridNode::northEdge(bathyGrid::stitchList2 *ns, int edgeSize, int edgeStart)
{
	int ct;

	if (!hasChildren)
	{
		ct = ns->count;
		ns->demStarts[ct] = edgeStart;
		ns->npts[ct] = dtm->nX;  
		ns->dtmSList[ct] = dtm;
		ns->count = ct + 1;
		return;
	}
	if (children[2] != NULL)
		children[2]->northEdge(ns, edgeSize / 2, edgeStart);
	if (children[3] != NULL)
		children[3]->northEdge(ns, edgeSize / 2, edgeStart + edgeSize / 2);
}

void metaGridNode:: getSWcorner(p3d &corner)
{
	if(children[0] != NULL)
		children[0]->getSWcorner(corner);
	else
		dtm->getSWcornerVal(corner);
}

void metaGridNode::printChildren()  //  for debugging
{
	int i;
	cerr << "NODE " << nodeID << " has children: ";

	for (i = 0; i<4; ++i)
	{
		if (children[i] != NULL)
		{
			cerr << children[i]->nodeID << " ";
		}
	}
	cerr << "\n";
	if (dtm != NULL)
	{
		cerr << "DATA sz " << dtm->nX << " " << dtm->nY << "\n";
		cerr << "LL lon lat " << dtm->LL_Lon << " " << dtm->LL_Lat << "\n";
	}
	//
	if (Esib != NULL)
		cerr << "Right : " << Esib->nodeID << " ";
	if (Wsib != NULL)
		cerr << "Left  : " << Wsib->nodeID << " ";
	if (Nsib != NULL)
		cerr << "Up  : " << Nsib->nodeID << " ";
	if (Ssib != NULL)
		cerr << "Down  : " << Ssib->nodeID << " ";
	cerr << "\n";

	for (i = 0; i<4; ++i)
	{
		if (children[i] != NULL)
		{
			children[i]->printChildren();
		}
	}

}

void metaGridNode::printStitchLists()  // for debugging
{
	int i;

	cerr << "\nSTITCH LISTS for NID " << nodeID << "\n";
	if (estitch != NULL)
	{
		cerr << " E estitch->count " << estitch->count << "\n";
		for (i = 0; i<estitch->count; ++i)
		{

			cerr << i << " ID " << estitch->dtmSList[i]->nodeID << " " << estitch->demStarts[i] << " ";
			cerr << estitch->npts[i] << "\n";
		}
	}

	if (wstitch != NULL)
	{
		cerr << " W wstitch->count " << wstitch->count << "\n";
		for (i = 0; i<wstitch->count; ++i)
		{

			cerr << i << " ID " << wstitch->dtmSList[i]->nodeID << " " << wstitch->demStarts[i] << " ";
			cerr << wstitch->npts[i] << "\n";
		}
	}


	if (nstitch != NULL)
	{
		cerr << " N nstitch->count " << nstitch->count << "\n";
		for (i = 0; i<nstitch->count; ++i)
		{

			cerr << i << " ID " << nstitch->dtmSList[i]->nodeID << " " << nstitch->demStarts[i] << " ";
			cerr << nstitch->npts[i] << "\n";
		}
	}

	if (sstitch != NULL)
	{
		cerr << "S sstitch->count " << sstitch->count << "\n";
		for (i = 0; i<sstitch->count; ++i)
		{

			cerr << i << " ID " << sstitch->dtmSList[i]->nodeID << " " << sstitch->demStarts[i] << " ";
			cerr << sstitch->npts[i] << "\n";
		}
	}

	for (i = 0; i<4; ++i)
	{
		if (children[i] != NULL)
		{
			children[i]->printStitchLists();
		}
	}
}

// for the special case stitch lists at the 72 and 80 deg boundaries
void metaGridNode::linkSouthEdge72_80(metaGridNode *s1, metaGridNode *s2, metaGridNode *s3)
{
	Ssib = s1;
	Ssib2 = s2;
	Ssib3 = s3;
}

// for the special case stitch lists at the 72 and 80 deg boundaries
void metaGridNode::gen_72_80_BorderStrip()
{
	bool hasSouthernSibs = false;
	southPointCount = 0;
	northPointCount = 0;  // For the northern edge of the southern neighbors

	if (Ssib != NULL) {
		hasSouthernSibs = true;
		Ssib->getNPoints(northPointList, northPointCount);
	}
	if (Ssib2 != NULL) {
		hasSouthernSibs = true;
		Ssib2->getNPoints(northPointList, northPointCount);
	}
	if (Ssib3 != NULL) {
		hasSouthernSibs = true;
		Ssib3->getNPoints(northPointList, northPointCount);
	}

	if (!hasSouthernSibs) return;  // no strip needed

	if (!hasChildren) // for the metagrid8root
		dtm->copySouthEdgeV(southPointList, southPointCount);
	if (children[0] != NULL)
		children[0]->getSPoints(southPointList, southPointCount);

	if (children[1] != NULL)
		children[1]->getSPoints(southPointList, southPointCount);

	genTriangleList();

}
// for the special case stitch lists at the 72 and 80 deg boundaries
void metaGridNode::getSPoints(vector<p3d> &southPointList, int &southPointCount)
{
	if (!hasChildren) //get South Edge Points
		dtm->copySouthEdgeV(southPointList, southPointCount); // also go for norms

	if (children[0] != NULL)
		children[0]->getSPoints(southPointList, southPointCount);

	if (children[1] != NULL)
		children[1]->getSPoints(southPointList, southPointCount);

}
// for the special case stitch lists at the 72 and 80 deg boundaries
void metaGridNode::getNPoints(vector<p3d> &northPointL, int &nPointCount)
{
	if (!hasChildren) //get North Edge Points
		dtm->copyNorthEdgeV(northPointL, nPointCount); // also go for norms

	if (children[2] != NULL)
		children[2]->getNPoints(northPointL, nPointCount);

	if (children[3] != NULL)
		children[3]->getNPoints(northPointL, nPointCount);

}

// for the special case stitch lists at the 72 and 80 deg boundaries
void metaGridNode::genTriangleList()
{
	int nNorth, nSouth;
	int i, in, is;
	p3d pn, ps, p3;
	triangle tri;
	float pnx, psx;
	bool done = false;
	bool isSouth;

	in = is = 0;

	nNorth = northPointList.size();

	nSouth = northPointList.size();

	while (!done)
	{
		pn = northPointList[in];
		ps = southPointList[is];
		// find nearest of 
		pnx = northPointList[in + 1].pt[0];
		psx = southPointList[is + 1].pt[0];
		if (pnx < psx)
		{
			p3 = northPointList[in + 1];
			isSouth = false;
		}
		else
		{
			p3 = southPointList[is + 1];
			isSouth = true;
		}
		//copy the verts
		for (i = 0; i<3; ++i) tri.verts[0][i] = pn.pt[i];
		for (i = 0; i<3; ++i) tri.verts[1][i] = ps.pt[i];
		for (i = 0; i<3; ++i) tri.verts[2][i] = p3.pt[i];
		// copy the norms 
		for (i = 0; i<3; ++i) tri.vertNorms[0][i] = pn.pn[i];
		for (i = 0; i<3; ++i) tri.vertNorms[1][i] = ps.pn[i];
		for (i = 0; i<3; ++i) tri.vertNorms[2][i] = p3.pn[i];
		// copy the attribs
		tri.att[0] = pn.attrib;
		tri.att[1] = ps.attrib;
		tri.att[2] = p3.attrib;

		triangleList.push_back(tri);

		// if new point is bottom replace bottom
		// if new point is top replace top
		if (isSouth) {
			ps = p3;
			++is;
		}
		else {
			pn = p3;
			++in;
		}
		if (in > nNorth - 2) done = true;
		if (is > nSouth - 2) done = true;

	}

}

// for the special case stitch lists at the 72 and 80 deg boundaries
void metaGridNode::drawTriangleList()
{
	int i, n;
	float tscale;
	float cmv;
	n = triangleList.size();
	if (n == 0)return;

	tscale = -TEX_SCALE / VERT_SCALE;

	glColor3f(1.0f, 1.0f, 1.0f);

	glBegin(GL_TRIANGLES);

	for (i = 0; i<n; ++i)
	{
		cmv = triangleList[i].att[0];
		glTexCoord2f(cmv, triangleList[i].verts[0][2] * tscale);
		glNormal3sv(triangleList[i].vertNorms[0]);
		glVertex3fv(triangleList[i].verts[0]);
		glNormal3sv(triangleList[i].vertNorms[1]);
		glVertex3fv(triangleList[i].verts[1]);
		glNormal3sv(triangleList[i].vertNorms[2]);
		glVertex3fv(triangleList[i].verts[2]);
	}

	glEnd();
}









